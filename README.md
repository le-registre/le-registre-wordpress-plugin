# Le Registre WordPress Plugin
Contributors: Marin Esnault (enomarin)
License: MIT

Le Registre WordPress Plugin create CPT and admin panels to use Hotel Pasteur Theme and Le Registre svelte web application.

## Description
Le Registre WordPress Plugin create CPT and admin panels to use Hotel Pasteur Theme and Le Registre svelte web application.
Its main role is to create custom post type (Les Séjours) and taxonomies (spaces, activies, host status) for use with Le Registre web application.
The plugin also create the host admin panels, which offer a limited and easy environment for host to document their journey.

## Installation
1. Unpack Le Registre Wordpress Plugin to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

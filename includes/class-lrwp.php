<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    LRWP
 * @subpackage LRWP/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    LRWP
 * @subpackage LRWP/includes
 * @author     Your Name <email@example.com>
 */
class LRWP
{

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      LRWP_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $lrwp    The string used to uniquely identify this plugin.
	 */
	protected $lrwp;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct()
	{
		if (defined('LRWP_VERSION')) {
			$this->version = LRWP_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->lrwp = 'lrwp';

		$this->load_dependencies();
		$this->define_admin_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - LRWP__Loader. Orchestrates the hooks of the plugin.
	 * - LRWP__i18n. Defines internationalization functionality.
	 * - LRWP__Admin. Defines all hooks for the admin area.
	 * - LRWP__Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies()
	{

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-lrwp-loader.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class-lrwp-admin.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-lrwp-cpt.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class-lrwp-page-patterns.php';

		$this->loader = new LRWP_Loader();
	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks()
	{

		$plugin_admin = new LRWP_Admin($this->get_lrwp(), $this->get_version());
		$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
		$this->loader->add_action('admin_menu', $plugin_admin, 'host_main_menu', 999);
		$this->loader->add_action('admin_bar_menu', $plugin_admin, 'host_admin_bar', 999);
		$this->loader->add_action('wp_dashboard_setup', $plugin_admin, 'host_widget_reset', 999);
		$this->loader->add_action('allowed_block_types', $plugin_admin, 'host_gutenberg_filter', 999);
		$this->loader->add_action('enqueue_block_editor_assets', $plugin_admin, 'custom_link_injection_to_gutenberg_toolbar', 999);
		$this->loader->add_action('pre_get_posts', $plugin_admin, 'query_set_only_author', 999);
		$this->loader->add_action('login_enqueue_scripts', $plugin_admin, 'login_enqueue_scripts', 999);
		$this->loader->add_action('load-edit.php',$plugin_admin,'wpse_trashed_redirect', 999);
		$this->loader->add_action('init', $plugin_admin, 'add_action_publish_if_updated', 1, 3);
		$this->loader->add_filter('manage_sejour_posts_columns', $plugin_admin, 'add_acf_columns');
		$this->loader->add_action ( 'manage_sejour_posts_custom_column', $plugin_admin, 'sejour_custom_column', 10, 2 );
		// TODO : move CPT creation to it's own function
		$plugin_post_types = new LRWP_CPT();
		$this->loader->add_action('init', $plugin_post_types, 'create_custom_post_types', 999);
		$this->loader->add_action('init', $plugin_post_types, 'create_custom_taxonomies', 999);
		// ADD custom page patterns
		$plugin_page_patterns = new LRWP_PAGE_PATTERNS();
		$this->loader->add_action('init', $plugin_page_patterns, 'add_page_patterns', 999);
		$this->loader->add_action('after_setup_theme', $plugin_page_patterns, 'remove_default_patterns', 999);
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run()
	{
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_lrwp()
	{
		return $this->lrwp;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    LRWP_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader()
	{
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version()
	{
		return $this->version;
	}
}

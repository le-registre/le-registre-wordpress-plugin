<?php

/**
 * 
 * 
 * 
 *
 * @package    LRWP_
 * @subpackage LRWP_/admin
 * @author     Your Name <email@example.com>
 */
class LRWP_Admin
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $lrwp    The ID of this plugin.
	 */
	private $lrwp;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $lrwp       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($lrwp, $version)
	{

		$this->lrwp = $lrwp;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * Enqueue specific styles for the hôte and conciergerie admin area.
		 */
		$user = wp_get_current_user();
		if (in_array('hote', $user->roles)) {
			wp_enqueue_style($this->lrwp, plugin_dir_url(__FILE__) . 'css/lrwp-admin-espace-hote.css', array(), $this->version, 'all');
		} else if (in_array('conciergerie', $user->roles)) {
			wp_enqueue_style($this->lrwp, plugin_dir_url(__FILE__) . 'css/lrwp-admin-conciergerie.css', array(), $this->version, 'all');
		}
		wp_enqueue_style($this->lrwp, plugin_dir_url(__FILE__) . 'css/lrwp-admin.css', array(), $this->version, 'all');
	}

	/**
	 * This function load the custom styles for the login and sign in pages (custom logo and page background)
	 */
	public function login_enqueue_scripts()
	{
		include('partials/lrwp-custom-login.php');
	}

	/**
	 * This function remove all the menu page and subpages for Hôte user.
	 */
	public function host_main_menu()
	{
		$user = wp_get_current_user();
		if (in_array('hote', $user->roles)) {
			remove_menu_page('index.php');
			remove_menu_page('jetpack');
			remove_menu_page('edit.php');
			remove_menu_page('upload.php');
			remove_menu_page('edit.php?post_type=page');
			remove_menu_page('edit.php?post_type=sejour');
			remove_menu_page('edit.php?post_type=rendez-vous');
			remove_menu_page('edit.php?post_type=histoire');
			remove_menu_page('edit-comments.php');
			remove_menu_page('themes.php');
			remove_menu_page('plugins.php');
			remove_menu_page('users.php');
			remove_menu_page('profile.php');
			remove_menu_page('tools.php');
			remove_menu_page('options-general.php');
			remove_submenu_page('index.php', 'relevanssi_admin_search');
			remove_submenu_page('edit.php?post_type=sejour', 'edit.php?post_type=sejour');
			remove_submenu_page('edit.php?post_type=sejour', 'post-new.php?post_type=sejour');
			remove_submenu_page('edit.php?post_type=rendez-vous', 'edit.php?post_type=rendez-vous');
			remove_submenu_page('edit.php?post_type=rendez-vous', 'post-new.php?post_type=rendez-vous');

			add_menu_page('espace-hote', 'Espace Hôte', 'read', 'index.php', '', '', 1);
		}
	}
	/**
	 * This function remove the unecessary menus for the host admin bar and add a logout button.
	 */
	public function host_admin_bar()
	{
		global $wp_admin_bar;
		$user = wp_get_current_user();
		if (in_array('hote', $user->roles)) {
			$wp_admin_bar->remove_menu('about');
			$wp_admin_bar->remove_menu('wp-logo');
			$wp_admin_bar->remove_menu('wporg');
			$wp_admin_bar->remove_menu('documentation');
			$wp_admin_bar->remove_menu('support-forums');
			$wp_admin_bar->remove_menu('feedback');
			$wp_admin_bar->remove_menu('dashboard');
			$wp_admin_bar->remove_menu('menus');
			$wp_admin_bar->remove_menu('widgets');
			$wp_admin_bar->remove_menu('themes');
			$wp_admin_bar->remove_menu('customize');
			$wp_admin_bar->remove_menu('updates');
			$wp_admin_bar->remove_menu('comments');
			$wp_admin_bar->remove_menu('new-content');
			$wp_admin_bar->remove_menu('new-post');
			$wp_admin_bar->remove_menu('new-media');
			$wp_admin_bar->remove_menu('new-page');
			$wp_admin_bar->remove_menu('new-user');
			$wp_admin_bar->remove_menu('edit');
			$wp_admin_bar->remove_menu('my-account');
			$wp_admin_bar->remove_menu('search');

			// Add a logout button
			$args = array(
				'id'    => 'logging-out',
				'title' => 'Se déconnecter',
				'href'   => '/wp-login.php?action=logout',
			);
			$wp_admin_bar->add_node($args);
		}
	}

	/**
	 * This function remove all the wigets for the hôte user.
	 */
	public function host_widget_reset()
	{
		$user = wp_get_current_user();
		if (in_array('hote', $user->roles)) {
			remove_meta_box('dashboard_activity', 'dashboard', 'normal');
			remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
			remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
			remove_meta_box('dashboard_primary', 'dashboard', 'side');
			remove_meta_box('pgc_sgb_dashboard_widget', 'dashboard', 'normal');
			remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal');
			wp_add_dashboard_widget(
				'host_custom_dashboard',                          // Widget slug.
				esc_html__('Mes séjours', 'wporg'), // Title.
				array($this, 'host_custom_dashboard')                    // Display function.
			);
		}
	}

	/** This function load our custom host dashboard  */
	public function host_custom_dashboard()
	{
		include('partials/lrwp-host-dashboard.php');
	}

	/*
	 * This function filter the guntenberg blocks for the "sejour" CPT
	 */
	public function host_gutenberg_filter($allowed_blocks)
	{
		global $post;
		if ($post->post_type == 'sejour') {
			return array(
				// We desactive all blocks for sejour  except audio, text, embed and image blocks
				'core/audio',
				'core/embed',
				'core/image',
				'core/gallery',
				'core/paragraph',
				'core/textColumns',
				'core/social-links',
				'core/social-link',
				'core/heading',
				'core/separator',
				'core/column',
				'core/columns'
			);
		} else {
			return $allowed_blocks;
		}
	}

	/** 
	 * This function add our custom hotel pasteur home button
	 * 
	 * It test for the screen and the user to be for sejour CPT screen and Hôte role.
	 * It enqueue the lrwp-gutenberg-home.js script at the footer of the gutenberg editor page.
	 * The script add a text button for the Hôte user to return to his dashboard. 
	 */
	function custom_link_injection_to_gutenberg_toolbar()
	{
		global $post;
		$screen = get_current_screen();
		$user = wp_get_current_user();
		if ('sejour' === $screen->post_type && in_array('hote', $user->roles)) {
			if (get_post_status($post) == 'publish') {
				wp_enqueue_script('lrwp-gutenberg-save', plugin_dir_url(__FILE__) . 'js/lrwp-gutenberg-save.js', array(), '1.0', true);
				wp_enqueue_script('lrwp-gutenberg-home', plugin_dir_url(__FILE__) . 'js/lrwp-gutenberg-home.js', array(), '1.0', true);
			} else {
				wp_enqueue_script('lrwp-gutenberg-home', plugin_dir_url(__FILE__) . 'js/lrwp-gutenberg-home.js', array(), '1.0', true);
			}
		}
	}
	/** This function disable showing posts and media of other Hôte users (but not acf-field and acf-field-group)*/
	public function query_set_only_author($wp_query)
	{
		global $current_user;
		$wp_query_post_type = $wp_query->get('post_type');

		if (in_array('hote', $current_user->roles)) {
			if (is_admin() && !current_user_can('edit_others_posts')) {
				if ($wp_query_post_type  !== "acf-field-group" && $wp_query_post_type  !== "acf-field") {
					$wp_query->set('author', $current_user->ID);
				}
			}
		}
	}
	/** This function add a redirection from the edit admin page to the dashboard */
	public function wpse_trashed_redirect()
	{
		$screen = get_current_screen();
		global $current_user;
		if (in_array('hote', $current_user->roles) && $screen->base === 'edit') {
			wp_safe_redirect('/wp-admin');
			exit();
		}
	}
	/** This function directly change the post status to published after first validation by the conciergerie */
	public function add_action_publish_if_updated()
	{
		function publish_if_updated($post_id, $post_after, $post_before)
		{
			global $current_user;
			if (in_array('hote', $current_user->roles) && get_post_status($post_before) == 'publish' && get_post_status($post_after) == 'pending' && get_post_type($post_id) == 'sejour') {
				remove_action('post_updated', 'publish_if_updated');
				wp_update_post(array('ID' => $post_id, 'post_status' => 'publish'));
				add_action('post_updated', 'publish_if_updated');
			}
		}
		add_action('post_updated', 'publish_if_updated', 10, 3);
	}

	/*
 * This function add columns to les sejours post list
 */
	function add_acf_columns($columns)
	{
		return array_merge($columns, array(
			'start_date' => __('Début du séjour'),
			'end_date'   => __('Fin du séjour')
		));
	}
	function sejour_custom_column($column, $post_id)
	{
		switch ($column) {
			case 'start_date':

				$start_date =  strtotime(get_post_meta($post_id, 'start_date', true));
				echo date_i18n("d F, Y", $start_date);
				break;
			case 'end_date':
				$is_end_date = get_field_object('is_end_date')['value'];
				if ($is_end_date == "true") {
					echo date_i18n("d F, Y", strtotime(get_field('end_date')));
				}
				break;
		}
	}
}

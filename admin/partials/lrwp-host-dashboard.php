<?php

/**
 * Provide a the "hote" user custom dashboard
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    LRWP
 * @subpackage LRWP/admin/partials
 */
global $current_user;
wp_get_current_user();
$custom_logo_id = get_theme_mod('custom_logo');
$image = wp_get_attachment_image_src($custom_logo_id, 'full');
?>
<div id="host-dashboard">
    <img src="<?php echo $image[0] ?>" alt="custom-logo"></img>
    <h1 id="title">Bienvenue sur votre espace hôte, <?php echo $current_user->display_name ?></h1>
    <?php
    $author_id = $current_user->ID;
    /* Query for pusblish sejour custom post type */
    $the_query = new WP_Query(array(
        'post_type' => 'sejour',
        'posts_per_page' => -1,
        'author' => $author_id,
        'meta_key' => 'start_date',
        'meta_type' => 'DATE',
        'order' => 'DESC',
        'orderby' => 'meta_value',
        'post_status' => ['publish']
    ));

    if ($the_query->have_posts()) {
        echo '<div class="visits-container">';
        echo '<h4>Séjour(s) validé(s) par la conciergerie</h4>';
        $url = get_option('siteurl');
        echo '<ul>';
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $id = get_the_ID();
            $date = get_the_date();
            $title = get_the_title();
            echo '<li>' . $date . ' – ' . $title . ' – <a href="' . $url . '/wp-admin/post.php?post=' . $id . '&action=edit">Modifier le séjour</a></li>';
        }
        echo '</ul>';
        echo '</div>';
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    /* Query for pending sejour custom post type */
    $the_query = new WP_Query(array(
        'post_type' => 'sejour',
        'posts_per_page' => -1,
        'author' => $author_id,
        'meta_key' => 'start_date',
        'meta_type' => 'DATE',
        'order' => 'DESC',
        'orderby' => 'meta_value',
        'post_status' => 'pending'
    ));

    if ($the_query->have_posts()) {
        echo '<div class="visits-container">';
        echo '<h4>Séjour(s) en cours de validation par la conciergerie</h4>';
        $url = get_option('siteurl');
        echo '<ul>';
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $id = get_the_ID();
            $date = get_the_date();
            $title = get_the_title();
            $modify_post_url = $url . '/wp-admin/post.php?post=' . $id . '&action=edit';
    ?>
            <li class="visit-row">
                <?php echo '<p>' . $date . '</p><h4>' . $title . '</h4> ' ?>
                <div class="visit-control">
                    <div class="host-button">
                        <span class="tooltip">Modifier le séjour </span>
                        <a href="<?php echo $modify_post_url ?>">
                            <button>
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M20.7,7C21.1,6.6 21.1,6 20.7,5.6L18.4,3.3C18,2.9 17.4,2.9 17,3.3L15.2,5.1L19,8.9M3,17.2V21H6.8L17.8,9.9L14.1,6.1L3,17.2M7,2V5H10V7H7V10H5V7H2V5H5V2H7Z" />
                                </svg>
                            </button>
                        </a>
                    </div>
                </div>
            </li>
        <?php
        }
        echo '</ul>';
        echo '</div>';
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    /* Query for draft sejour custom post type */
    $the_query = new WP_Query(array(
        'post_type' => 'sejour',
        'posts_per_page' => -1,
        'author' => $author_id,
        'meta_key' => 'start_date',
        'meta_type' => 'DATE',
        'order' => 'DESC',
        'orderby' => 'meta_value',
        'post_status' => 'draft'
    ));

    if ($the_query->have_posts()) {
        echo '<div class="visits-container">';
        echo '<h4>Séjour(s) en brouillon</h4>';
        $url = get_option('siteurl');
        echo '<ul>';
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $id = get_the_ID();
            $date = get_the_date();
            $title = get_the_title();
            $modify_post_url = $url . '/wp-admin/post.php?post=' . $id . '&action=edit';
            $trash_bare_url = $url . "/wp-admin/post.php?post=" . $id . "&action=trash";
            $trash_post_url = wp_nonce_url($trash_bare_url, 'trash-post_' . $id);
        ?>
            <li class="visit-row">
                <?php echo '<p>' . $date . '</p><h4>' . $title . '</h4> ' ?>
                <div class="visit-control">
                    <div class="host-button">
                        <span class="tooltip">Modifier le séjour </span>
                        <a href="<?php echo $modify_post_url ?>">
                            <button>
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M20.7,7C21.1,6.6 21.1,6 20.7,5.6L18.4,3.3C18,2.9 17.4,2.9 17,3.3L15.2,5.1L19,8.9M3,17.2V21H6.8L17.8,9.9L14.1,6.1L3,17.2M7,2V5H10V7H7V10H5V7H2V5H5V2H7Z" />
                                </svg>
                            </button>
                        </a>
                    </div>
                    <div class="host-button">
                        <span class="tooltip">Supprimer le séjour</b> </span>
                        <a href="<?php echo $trash_post_url ?>">
                            <button>
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" />
                                </svg>
                            </button>
                        </a>
                    </div>
                </div>
            </li>
        <?php
        }
        echo '</ul>';
        echo '</div>';
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    /* Query for trashed sejour custom post type */
    $the_query = new WP_Query(array(
        'post_type' => 'sejour',
        'posts_per_page' => -1,
        'author' => $author_id,
        'meta_key' => 'start_date',
        'meta_type' => 'DATE',
        'order' => 'DESC',
        'orderby' => 'meta_value',
        'post_status' => 'trash'
    ));

    if ($the_query->have_posts()) {
        echo '<div class="visits-container">';
        echo '<h4>Séjour(s) supprimés</h4>';
        $url = get_option('siteurl');
        echo '<ul>';
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $id = get_the_ID();
            $date = get_the_date();
            $title = get_the_title();
            $untrash_bare_url = $url . "/wp-admin/post.php?post=" . $id . "&action=untrash";
            $untrash_post_url = wp_nonce_url($untrash_bare_url, 'untrash-post_' . $id);

            $delete_bare_url = $url . "/wp-admin/post.php?post=" . $id . "&action=delete";
            $delete_post_url = wp_nonce_url($delete_bare_url, 'delete-post_' . $id);
        ?>
            <li class="visit-row">
                <?php echo '<p>' . $date . '</p><h4>' . $title . '</h4> ' ?>
                <div class="visit-control">
                    <div class="host-button">
                        <span class="tooltip">Sortir de la corbeille </span>
                        <a href="<?php echo $untrash_post_url ?>">
                            <button>
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M14,14H16L12,10L8,14H10V18H14V14M6,7H18V19C18,19.5 17.8,20 17.39,20.39C17,20.8 16.5,21 16,21H8C7.5,21 7,20.8 6.61,20.39C6.2,20 6,19.5 6,19V7M19,4V6H5V4H8.5L9.5,3H14.5L15.5,4H19Z" />
                                </svg>
                            </button>
                        </a>
                    </div>
                    <div class="host-button">
                        <span class="tooltip">Supprimer <b>définitivement</b> </span>
                        <a href="<?php echo $delete_post_url ?>">
                            <button>
                                <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                                    <path fill="currentColor" d="M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19M8.46,11.88L9.87,10.47L12,12.59L14.12,10.47L15.53,11.88L13.41,14L15.53,16.12L14.12,17.53L12,15.41L9.88,17.53L8.47,16.12L10.59,14L8.46,11.88M15.5,4L14.5,3H9.5L8.5,4H5V6H19V4H15.5Z" />
                                </svg>
                            </button>
                        </a>
                    </div>
                </div>
            </li>
    <?php
        }
        echo '</ul>';
        echo '</div>';
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    /* Query for other sejour custom post type */
    $the_query = new WP_Query(array(
        'post_type' => 'sejour',
        'posts_per_page' => -1,
        'author' => $author_id,
        'meta_key' => 'start_date',
        'meta_type' => 'DATE',
        'order' => 'DESC',
        'orderby' => 'meta_value',
        'post_status' => ['auto-draft', 'future', 'private']
    ));

    if ($the_query->have_posts()) {
        echo '<div class="visits-container">';
        echo '<h4>Autre(s) séjour(s)</h4>';
        $url = get_option('siteurl');
        echo '<ul>';
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $id = get_the_ID();
            $date = get_the_date();
            $title = get_the_title();
            echo '<li>' . $date . ' – ' . $title . ' – <a href="' . $url . '/wp-admin/post.php?post=' . $id . '&action=edit">Modifier le séjour</a></li>';
        }
        echo '</ul>';
        echo '</div>';
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();

    /* Link to add a sejour custom post type */
    ?>
    <div id="host-footer-bar">
        <div class="host-button">
            <span class="tooltip">Ajouter un séjour</span>
            <a href="/wp-admin/post-new.php?post_type=sejour">
                <button>
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M12,20C7.59,20 4,16.41 4,12C4,7.59 7.59,4 12,4C16.41,4 20,7.59 20,12C20,16.41 16.41,20 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M13,7H11V11H7V13H11V17H13V13H17V11H13V7Z" />
                    </svg>
                </button>
            </a>
        </div>
        <div class="host-button">
            <span class="tooltip">Se déconnecter</span>
            <a href="/wp-login.php?action=logout">
                <button>
                    <svg style="width:24px;height:24px" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M14.08,15.59L16.67,13H7V11H16.67L14.08,8.41L15.5,7L20.5,12L15.5,17L14.08,15.59M19,3A2,2 0 0,1 21,5V9.67L19,7.67V5H5V19H19V16.33L21,14.33V19A2,2 0 0,1 19,21H5C3.89,21 3,20.1 3,19V5C3,3.89 3.89,3 5,3H19Z" />
                    </svg>
                </button>
            </a>
        </div>
    </div>
</div>
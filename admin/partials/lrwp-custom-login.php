<?php
// Get custom logo url
$custom_logo_id = get_theme_mod('custom_logo');
$image = wp_get_attachment_image_src($custom_logo_id, 'full');
?>
<style type="text/css">
    body:before {
        background-image: linear-gradient(#fff0 0%, #ffff 50%),
            url("/wp-content/themes/hotel-pasteur-theme/le-registre/public/assets/background-image.webp");
        background-repeat: no-repeat;
        background-blend-mode: overlay;
        content: " ";
        display: block;
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        opacity: 0.5;
        background-repeat: no-repeat;
        background-position: 50% 0;
        background-size: cover;
        z-index: -99;
    }

    #login h1 a,
    .login h1 a {
        background-image: url("<?php echo $image[0] ?>");
    }
    #login #nav a:first-child {
        padding: 5px 5px 5px;
        font-size: 1rem;
        font-weight: bold;
    }
</style>
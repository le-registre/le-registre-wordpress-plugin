// custom-link-in-toolbar.js
/* Credits to Mikhail.root (https://stackoverflow.com/questions/62105561/adding-button-to-header-of-gutenberg-editor-in-wordpress-through-plugin)*/
// wrapped into IIFE - to leave global space clean.
(function (window, wp) {
  // just to keep it cleaner - we refer to our link by id for speed of lookup on DOM.
  var link_id = "return-to-dashboard";
  // prepare our custom link's html.
  var link_html =
    '<a id="' +
    link_id +
    '" class="components-button is-tertiary" href="/wp-admin/" style="margin-left: 10px;">Retourner à l\'espace hôte</a>';

  // check if gutenberg's editor root element is present.
  var editorEl = document.getElementById("editor");
  if (!editorEl) {
    // do nothing if there's no gutenberg root element on page.
    return;
  }

  var unsubscribe = wp.data.subscribe(function () {
    setTimeout(function () {
      if (!document.getElementById(link_id)) {
        var toolbalEl = editorEl.querySelector(".edit-post-header");
        if (toolbalEl instanceof HTMLElement) {
          toolbalEl.insertAdjacentHTML("afterbegin", link_html);
        }
      }
      let titleArea = document.getElementById("post-title-0");
      if (titleArea instanceof HTMLElement) {
        titleArea.placeholder = "Saisissez le titre de votre séjour";
      }

      if (document.getElementsByClassName("is-root-container")[0]) {
        let defaultBlockAppender = document.getElementsByClassName('block-editor-default-block-appender')[0]
        if(defaultBlockAppender && defaultBlockAppender instanceof HTMLElement) {
          defaultBlockAppender.firstChild.textContent = "Cliquez ici pour ajouter du contenu sur la page de votre séjour"
        }
        let emptyBlocArea =
          document.getElementsByClassName("is-root-container")[0].childNodes;
        if (emptyBlocArea instanceof NodeList) {
          emptyBlocArea.forEach((node) => {
            if (node.dataset.empty == "true" && node.classList.contains('is-selected')) {
              node.lastElementChild.dataset.richTextPlaceholder = `Utilisez le bouton + à droite pour ajouter des blocs de mise en page`;
            }
            else if (node.dataset.empty == "true") {
              node.lastElementChild.dataset.richTextPlaceholder = "Cliquez ici pour ajouter du contenu sur la page de votre séjour";
            }
          });
        }
      }
    }, 1);
  });
  // unsubscribe is a function - it's not used right now
  // but in case you'll need to stop this link from being reappeared at any point you can just call unsubscribe();
})(window, wp);

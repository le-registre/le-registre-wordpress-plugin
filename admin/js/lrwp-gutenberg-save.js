// custom-link-in-toolbar.js
/* Credits to Mikhail.root (https://stackoverflow.com/questions/62105561/adding-button-to-header-of-gutenberg-editor-in-wordpress-through-plugin)*/
// wrapped into IIFE - to leave global space clean.
(function (window, wp) {
  // check if gutenberg's editor root element is present.
  var editorEl = document.getElementById("editor");
  if (!editorEl) {
    // do nothing if there's no gutenberg root element on page.
    return;
  }
  var unsubscribe = wp.data.subscribe(function () {
    setTimeout(function () {
      var toolbalEl = editorEl.querySelector(".editor-post-publish-button");
      if (toolbalEl instanceof HTMLElement) {
        console.log(toolbalEl.innerText);
        toolbalEl.innerText = "Publier sur le registre";
      }
    }, 1);
  });
  // unsubscribe is a function - it's not used right now
  // but in case you'll need to stop this link from being reappeared at any point you can just call unsubscribe();
})(window, wp);
